package com.glearning.orders;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OrdersRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(OrdersRestApiApplication.class, args);
		
	}

}
