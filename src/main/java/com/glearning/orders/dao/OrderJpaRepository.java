package com.glearning.orders.dao;

import java.util.Optional;
import java.util.Set;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.glearning.orders.model.Order;

@Repository
public interface OrderJpaRepository extends JpaRepository<Order, Long>{
	
	Set<Order> findByAmountBetween(double min, double max);
	
	Set<Order> findByName(String name);
	
	Set<Order> findByAmountLessThan(double max);
	
	@Query("select o from Order o where o.email = ?1")
	Optional<Order> findByEmail(String email);

}
