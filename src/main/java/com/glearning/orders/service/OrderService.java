package com.glearning.orders.service;

import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glearning.orders.dao.OrderJpaRepository;
import com.glearning.orders.model.Order;

@Service
public class OrderService {
	
	@Autowired
	private OrderJpaRepository orderRepository;
	
	
	public Order saveOrder(Order order) {
		return this.orderRepository.save(order);
	}
	
	public Order updateOrder(long id, Order order) {
		Order existingOrder = this.orderRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("invalid order id"));
		existingOrder.setName(order.getName());
		existingOrder.setAmount(order.getAmount());
		return this.orderRepository.save(existingOrder);
	}
	
	public Set<Order> fetchAllOrders(){
		return Set.copyOf(this.orderRepository.findAll());
	}
	
	public Order findOrderById(long id) {
		return this.orderRepository.findById(id).orElseThrow(() -> new IllegalArgumentException("invalid order id passed"));
	}
	
	public void deleteOrderById(long id) {
		this.orderRepository.deleteById(id);
	}

}
